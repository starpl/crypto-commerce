import { Request, Response, NextFunction } from 'express';
import moment from 'moment';

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export const logger = (req: Request, res: Response, next: NextFunction) => {
    console.log(`${req.protocol}://${req.hostname}${req.originalUrl} : ${moment().format()}`);
    next();
};
