import crypto from 'crypto';
import { Request } from 'express';

export const hash = (pwd: string): string => {
    const hash = crypto.createHash('sha256');
    return hash.update(pwd).digest('hex');
};

export const genPassword = (password: string): { salt: string; hash: string } => {
    const salt = crypto.randomBytes(32).toString('hex');
    const genHash = crypto.pbkdf2Sync(password, salt, 10000, 64, 'sha512').toString('hex');
    return {
        salt: salt,
        hash: genHash,
    };
};

export const validPassword = (password: string, hash: string, salt: string): boolean => {
    const hashVerify = crypto.pbkdf2Sync(password, salt, 10000, 64, 'sha512').toString('hex');
    return hash === hashVerify;
};

// Types
export type RequestWithBody<RS> = Request<any, any, RS>;

export interface IProductDetails {
    name: string;
    quantity: number;
    amount: string;
    notes: string;
}

export interface IPaymentIntentBody {
    order_id: number;
    api_key: string;
    order_details: IProductDetails[];
}

export interface IWalletDataBody {
    address: string;
    pk: string;
    mode: string;
}

export interface IPaymentIntent {
    id: string;
    seller_id: string;
    order_id: number;
    order_details: IProductDetails[];
    amount: string;
    status: string;
    client_secret: string;
    tx: string | null;
}

export interface IUser {
    id: string;
    login: string;
    password: string;
    displayName: string;
    salt: string;
    timestamp: number;
    api_key: string;
    address: string;
    redirect_success: string;
    redirect_error: string;
}
