import express from 'express';
import path from 'path';
import exphbs from 'express-handlebars';
import session from 'express-session';
import pgConnect from 'connect-pg-simple';
import { config } from 'dotenv';
import passport from 'passport';
import fetch from 'node-fetch';
import { web3 } from './config/web3client';
import BN from 'bn.js';

// Utils
import { connectionConfig, prisma } from './config/database';

// Models
import { Users } from './models/Users';

// Middlewares
import { logger } from './middleware/logger';
import { isAuth } from './middleware/isAuth';

// Routers
import { registerRouter } from './routes/api/register';
import { paymentRouter } from './routes/api/payment';
import { loginRouter } from './routes/api/login';
import { IProductDetails } from './utils';
import { getBal, generateWallet, initializeWallet, tonweb, sendGrams } from './config/ton';

const app = express();
config();
const port = process.env.PORT || '8000';

require('./config/passport');

const Handlebars = exphbs.create({
    defaultLayout: 'main',
    helpers: {
        setChecked: (value, currentValue) => {
            if (value == currentValue) {
                return 'checked';
            } else {
                return '';
            }
        },
    },
});

// Middleware logger
app.use(logger);

// Handlebars middleware
app.engine('handlebars', Handlebars.engine);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', '.handlebars');

app.use(express.json());

// to handle url encoded data
app.use(express.urlencoded({ extended: false }));

const sessionStore = new (pgConnect(session))({
    // @ts-ignore
    conObject: connectionConfig,
});

app.use(
    session({
        // @ts-ignore
        secret: process.env.SECRET,
        resave: false,
        saveUninitialized: true,
        store: sessionStore,
        cookie: {
            maxAge: 1000 * 60 * 60, // Equals 1 hr (60 min/1 hr * 60 sec/1 min * 1000 ms / 1 sec)
        },
    }),
);

app.use(passport.initialize());
app.use(passport.session());

// Pages
app.get('/register', (req, res) => res.render('register', { title: 'Register', Users }));
// TODO: add login screen skip if authorized
app.get('/login', (req, res) => res.render('login', { title: 'Login screen' }));
app.get('/payment/:client_secret', async (request, response) => {
    const orders = await prisma.orders.findMany({
        where: {
            client_secret: request.params.client_secret,
        },
    });

    if (orders.length > 0 && orders[0].status === 'pending') {
        const prices = await (await fetch('https://ethgasstation.info/json/ethgasAPI.json')).json();
        const user = await prisma.users.findOne({
            where: {
                id: orders[0].seller_id,
            },
        });

        const order_details: IProductDetails[] = [];
        const parsed_details = JSON.parse(orders[0].order_details);
        for (const index in Object.keys(parsed_details)) {
            order_details.push(parsed_details[index]);
        }

        response.render('payment', {
            order_id: orders[0].order_id,
            displayName: user?.displayName,
            client_secret: orders[0].client_secret,
            order_details: [
                ...order_details.map((order, index) => ({
                    ...order,
                    // @ts-ignore
                    amount:
                        orders[0].cryptocurrency === 'ETH'
                            ? web3.utils.fromWei(new BN(order?.amount))
                            : tonweb.utils.fromNano(new BN(order.amount)),
                    sum:
                        orders[0].cryptocurrency === 'ETH'
                            ? web3.utils.fromWei(new BN(order?.amount).mul(new BN(order?.quantity)))
                            : tonweb.utils.fromNano(new BN(order.amount).mul(new BN(order.quantity))),
                    index: index + 1,
                })),
            ],
            total:
                orders[0].cryptocurrency === 'ETH'
                    ? web3.utils.fromWei(orders[0].amount)
                    : tonweb.utils.fromNano(orders[0].amount),
            mode: 'average',
            mode_slow: web3.utils.fromWei(web3.utils.toWei(new BN(prices.safeLow), 'gwei'), 'ether'),
            mode_average: web3.utils.fromWei(web3.utils.toWei(new BN(prices.average), 'gwei'), 'ether'),
            mode_fastest: web3.utils.fromWei(web3.utils.toWei(new BN(prices.fastest), 'gwei'), 'ether'),
            cryptocurrency: orders[0].cryptocurrency,
            isETH: orders[0].cryptocurrency === 'ETH',
        });
    } else response.status(404).send();
});

// Protected pages
app.get('/profile', isAuth, (req, res) => {
    res.render('profile');
});
app.get('/logout', isAuth, (req, res) => {
    req.logout();
    res.redirect('/login');
});

// API
app.use('/api/login', passport.authenticate('local', { failureRedirect: '/login' }), loginRouter);
app.use('/api/register', registerRouter);
app.use('/api/payments', paymentRouter);

app.listen(port, async (err) => {
    if (err) return console.error(err);
    return console.log(`Server is listening on ${port}`);
});
