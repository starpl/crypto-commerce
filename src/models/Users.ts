import { hash, IUser } from '../utils';

const testUsers: IUser[] = [
    {
        id: '1',
        displayName: 'Test store',
        login: 'admin',
        password: hash('password'),
        salt: '123',
        timestamp: 1590326337579,
        api_key: '123',
        address: '0xC90a8612333f40631Dc0756Ac2d9F07536CE466B',
        redirect_success: 'https://google.com',
        redirect_error: 'http://localhost:8000/',
    },
];

export { testUsers as Users };
