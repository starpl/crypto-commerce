import express from 'express';
import crypto from 'crypto';
const registerRouter = express.Router();
import { genPassword } from '../../utils';
import { prisma } from '../../config/database';

// Add new user
registerRouter.post('/', async (request, response) => {
    console.log(request.body);
    if (!request.body.login) {
        response.redirect('/register');
        return;
    }

    const user = await prisma.users.findMany({
        where: {
            login: {
                equals: request.body.login,
            },
        },
    });
    console.log(user);

    if (user.length === 0) {
        const { hash, salt } = genPassword(request.body.password);

        await prisma.users.create({
            data: {
                address: request.body.address,
                api_key: crypto.randomBytes(32).toString('hex'),
                displayName: request.body.name,
                login: request.body.login,
                password: hash,
                salt,
                timestamp: new Date().toISOString(),
                redirect_success: request.body.redirect_success,
                redirect_error: request.body.redirect_error,
            },
        });

        response.status(200).json({ msg: 'ok' });
        return;
    }
    response.redirect('/register');
});

export { registerRouter };
