import express from 'express';

const loginRouter = express.Router();

// Login
loginRouter.post('/', (req, res) => {
    console.log(req.body);
    res.redirect('/profile');
});

export { loginRouter };
