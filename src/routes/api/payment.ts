import express from 'express';
import crypto from 'crypto';
import { RequestWithBody, hash, IWalletDataBody, IProductDetails } from '../../utils';

import { sendETH } from '../../config/web3client';
import BN from 'bn.js';
import { prisma } from '../../config/database';
import { sendGrams, tonweb } from '../../config/ton';
import fetch from 'node-fetch';

const paymentRouter = express.Router();

// Add new paymentIntent
paymentRouter.post('/', async (request, response) => {
    if (request.body.cryptocurrency in ['GRAM', 'ETH']) {
        response.status(401).json({ msg: 'unknown_currency' });
        return;
    }

    const seller = await prisma.users.findMany({
        where: {
            api_key: request.body.api_key,
        },
    });

    if (seller.length > 0) {
        const order_details = {};
        for (const entry in request.body.order_details) {
            order_details[entry] = request.body.order_details[entry];
        }
        console.log(request.body.order_details);
        const amount = request.body.order_details
            .reduce(
                (acc: BN, curr: IProductDetails) => acc.add(new BN(curr.amount).mul(new BN(curr.quantity))),
                new BN(0),
            )
            .toString();

        console.log(amount);
        await prisma.orders.create({
            data: {
                amount,
                client_secret: hash(crypto.randomBytes(32).toString('hex')),
                order_id: request.body.order_id,
                status: 'pending',
                tx: null,
                users: { connect: { id: seller[0].id } },
                order_details: JSON.stringify(order_details),
                cryptocurrency: request.body.cryptocurrency,
            },
        });
        response.status(201).json({ msg: 'ok' });
    } else {
        response.status(401).json({ msg: 'incorrect api_key' });
    }
});

paymentRouter.post('/:client_secret', async (request: RequestWithBody<IWalletDataBody>, response) => {
    console.log(JSON.stringify(request.body, null, 4));

    const orders = await prisma.orders.findMany({
        where: {
            client_secret: request.params.client_secret,
        },
    });

    if (orders.length > 0) {
        if (
            request.body.address === '' ||
            request.body.pk === '' ||
            (!request.body.mode && orders[0].cryptocurrency === 'ETH')
        ) {
            // replace with err
            response.status(400).json({ msg: 'bad input' });
        } else if (orders[0].status !== 'success') {
            const seller = await prisma.users.findOne({ where: { id: orders[0].seller_id } });
            if (seller && seller.address) {
                let result;
                if (orders[0].cryptocurrency === 'ETH') {
                    result = await sendETH(
                        request.body.address,
                        request.body.pk,
                        seller.address,
                        orders[0].amount,
                        orders[0].id,
                        request.body.mode,
                    );
                } else {
                    result = await sendGrams(
                        request.body.address,
                        tonweb.utils.hexToBytes(request.body.pk),
                        orders[0].amount,
                        seller.address,
                        '',
                        orders[0].id,
                    );
                }

                console.log(result);
                if (!result.isSuccess) {
                    await fetch(seller.redirect_error, {
                        method: 'POST',
                        body: JSON.stringify({
                            status: 'error',
                            tx: result.txHash,
                        }),
                    });
                    response.redirect(seller.redirect_error);
                } else {
                    await fetch(seller.redirect_error, {
                        method: 'POST',
                        body: JSON.stringify({
                            status: 'success',
                            tx: result.txHash,
                        }),
                    });
                    response.redirect(seller.redirect_success);
                }
            } else {
                response.status(404).json({ msg: 'user not found' });
            }
        }
    } else {
        response.status(404).json({ msg: 'order not found' });
    }
});

export { paymentRouter };
