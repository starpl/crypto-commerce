import Tonweb from 'tonweb';
import { prisma } from './database';
import BN from 'bn.js';
const nacl = Tonweb.utils.nacl;

export const tonweb = new Tonweb();

export const generateWallet = async () => {
    const keyPair = nacl.sign.keyPair();
    console.log(keyPair);
    const wallet = tonweb.wallet.create({ publicKey: keyPair.publicKey });
    // const wallet = tonweb.wallet.create({ address: 'EQBQaSDGtdbiJYw5905As-F90qduE5Sae-sgNKsEMVU9oV4G' });
    const address = await wallet.getAddress();
    console.log(
        `New wallet generated, address: ${address}, non-bouncable: ${address.toString(
            true,
            true,
            false,
        )}, bouncable: ${address.toString(true, true, true, false)}`,
    );
    const pkRaw = keyPair.publicKey;
    const pkHex = tonweb.utils.bytesToHex(pkRaw);
    const skRaw = keyPair.secretKey;
    const skHex = tonweb.utils.bytesToHex(keyPair.secretKey);
    console.log(`HEX PK: ${pkHex},\nRAW PK: ${pkRaw}`);
    console.log(`HEX SK: ${skHex},\nRAW SK: ${skRaw}`);
    return { keyPair, address };
};

export const initializeWallet = async (address: any, pk: Uint8Array, sk: Uint8Array): Promise<void> => {
    const wallet = tonweb.wallet.create({ publicKey: pk });
    try {
        const seqno = await wallet.methods.seqno().call();
        // DEPLOY

        const deploy = wallet.deploy(sk); // deploy method
        // const deployFee = await deploy.estimateFee(); // get estimate fee of deploy doesnt work

        const deploySended = await deploy.send(); // deploy wallet contract to blockchain
        console.log(deploySended);

        // const deployQuery = await deploy.getQuery(); // get deploy query Cell
        // console.log(deployQuery);
        const transfer = wallet.methods.transfer({
            secretKey: sk,
            toAddress: address.toString(true, true, true, false),
            amount: Tonweb.utils.toNano(0.01), // 0.01 Gram
            seqno: seqno,
            payload: 'Hello',
            sendMode: 3,
        });

        const Cell = Tonweb.boc.Cell;
        const cell = new Cell();
        cell.bits.writeUint(0, 32);
        cell.bits.writeAddress(address);
        cell.bits.writeGrams(1);
        console.log(cell.print()); // print cell data like Fift
        const bocBytes = cell.toBoc();
        const history = await tonweb.getTransactions(address);
        console.log(history);

        const balance = await tonweb.getBalance(address);
        console.log(balance);

        tonweb.sendBoc(bocBytes);
        // const transferFee = await transfer.estimateFee(); // get estimate fee of transfer doesnt work

        const transferSended = await transfer.send(); // send transfer query to blockchain
        console.log(transferSended);
        // const transferQuery = await transfer.getQuery();

        // console.log(transferQuery);
    } catch (e) {
        console.log(`ERROR: ${e}`);
    }
};

export const getBal = async (address: string) => console.log(await tonweb.getBalance(address));

export const sendGrams = async (
    address: string,
    sk: Uint8Array,
    amount: string,
    to: string,
    message: string,
    id: number,
) => {
    const walletFrom = tonweb.wallet.create({ address });
    const walletTo = tonweb.wallet.create({ address: to });
    const walletToAddr = await walletTo.getAddress();
    const amountInNano = new BN(amount);
    let txHash = '';
    let isSuccess = true;
    let errorMessage = '';
    try {
        const seqno = await walletFrom.methods.seqno().call();
        const transfer = walletFrom.methods.transfer({
            secretKey: sk,
            toAddress: walletToAddr.toString(true, true, true, false),
            amount: amountInNano, // 0.01 Gram
            seqno: seqno,
            payload: message,
            sendMode: 3,
        });
        const transferSended = await transfer.send(); // send transfer query to blockchain
        console.log(transferSended);
        if (transferSended['@type'] !== 'ok') {
            isSuccess = false;
            errorMessage = 'error';
        } else if (transferSended['@type'] === 'ok') {
            console.log('Status: Approved');
            const lastTx: any[] = await tonweb.getTransactions(await walletFrom.getAddress(), 100);
            console.log(
                lastTx.map((tx) => tx.transaction_id.hash),
                lastTx[0],
                lastTx[0].transaction_id.hash,
            );
            txHash = lastTx[0].transaction_id.hash;
        }
    } catch (e) {
        console.log(`ERROR: ${e}`);
    }
    return { isSuccess, errorMessage, txHash };
};

// const wallet = tonweb.wallet.create({ address: 'EQBQaSDGtdbiJYw5905As-F90qduE5Sae-sgNKsEMVU9oV4G' });
