import { Pool } from 'pg';
import { PrismaClient } from '@prisma/client';

export const prisma = new PrismaClient();

require('dotenv').config();

export const connectionConfig = {
    host: process.env.HOST,
    database: process.env.DATABASE,
    user: process.env.USER,
    password: process.env.PASSWORD,
    port: process.env.DB_PORT,
};
// @ts-ignore
const pool = new Pool(connectionConfig);

export { pool };

// postgresql://postgres:root@localhost:5432/crypto-commerce
