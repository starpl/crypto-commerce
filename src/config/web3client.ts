import Web3 from 'web3';
import { TransactionConfig } from 'web3-core';
import fetch from 'node-fetch';
import BN from 'bn.js';
import { prisma } from './database';

require('dotenv').config();

// @ts-ignore
export const web3 = new Web3(process.env.WEB3_HOST);

const sendETH = async (from: string, pk: string, to: string, amount: string, id: number, mode: string) => {
    const prices = await (await fetch('https://ethgasstation.info/json/ethgasAPI.json')).json();
    const amountInWei = new BN(amount);
    const gasPrice = prices[mode];
    let isSuccess = true;
    let txHash = '';
    let errorMessage = '';
    try {
        const tx: TransactionConfig = {
            from,
            to,
            gasPrice,
            value: amountInWei,
        };
        const gasLimit = await web3.eth.estimateGas(tx);
        const transactionFee = new BN(gasLimit).mul(new BN(gasPrice));
        const fromBalance = new BN(await web3.eth.getBalance(from));
        console.log(
            `Balance: ${fromBalance}, amount to deduct: ${web3.utils
                .toWei(amount, 'ether')
                .toString()}, fee: ${transactionFee}, gasAmount: ${gasLimit}, gasPrice: ${gasPrice}`,
        );
        const difference = fromBalance.sub(amountInWei.add(transactionFee));
        if (difference < new BN(0)) {
            return { isSuccess: false, errorMessage: 'Not enough funds to complete transaction' };
        }

        tx.gas = gasLimit;

        const signed = await web3.eth.accounts.signTransaction(tx, pk);
        // @ts-ignore
        const transaction = web3.eth.sendSignedTransaction(signed.rawTransaction);

        // redirects
        transaction.once('transactionHash', async (hash) => {
            console.log('hash');
            console.log(hash);
            txHash = hash;
            await prisma.orders.update({
                where: {
                    id: id,
                },
                data: {
                    status: 'processing',
                    tx: hash,
                },
            });
        });

        transaction.once('receipt', async (receipt) => {
            console.log('reciept');
            console.log(receipt);
            web3.eth.getBalance(from).then(console.log);
            web3.eth.getBalance(to).then(console.log);
            console.log(gasPrice, gasLimit, transactionFee);
            await prisma.orders.update({
                where: {
                    id: id,
                },
                data: {
                    status: 'success',
                },
            });
        });
    } catch (e) {
        console.log(`ERROR HAPPENED INSIDE: ${e}`);
        errorMessage = e;
        isSuccess = false;
    }
    return { isSuccess, errorMessage, txHash };
};

export { sendETH };

// https://ethereum.stackexchange.com/questions/19665/how-to-calculate-transaction-fee
// https://stackoverflow.com/questions/46611117/how-to-authenticate-and-send-contract-method-using-web3-js-1-0
