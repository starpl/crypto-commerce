import passport from 'passport';
import { Strategy as LocalStrategy } from 'passport-local';
import { Users } from '../models/Users';
import { validPassword, IUser } from '../utils';

const verifyCallback = (username, password, done) => {
    const user = Users.find((user) => user.login === username);

    if (user !== undefined) {
        const isValid = validPassword(password, user.password, user.salt);
        if (isValid) {
            return done(null, user);
        } else {
            return done(null, false);
        }
    } else return done(null, false);
};

const strategy = new LocalStrategy(
    {
        usernameField: 'login',
        passwordField: 'password',
    },
    verifyCallback,
);

passport.use(strategy);
passport.serializeUser((user: IUser, done) => {
    done(null, user.id);
});

passport.deserializeUser((userId, done) => {
    const selected_user = Users.find((user) => user.id === userId);
    done(null, selected_user);
});
