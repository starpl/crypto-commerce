import {TONClient} from 'ton-client-node-js'

const availableNetworks = {
    local: 'http://localhost',
    testnet: 'https://testnet.ton.dev/',
};

const network = 'testnet';

const createClientInstance = async () => {
    try {
        const client = await TONClient.create({
            servers: availableNetworks[network],
            transactionTimeout: 30000,
        });
        console.log(`Connected to ${availableNetworks[network]}`);
        return client;
    } catch (error) {
        console.error(error);
    }
};

const client = createClientInstance();

export default client;
